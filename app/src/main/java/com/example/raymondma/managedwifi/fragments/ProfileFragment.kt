package com.example.raymondma.managedwifi.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.example.raymondma.managedwifi.R
import com.example.raymondma.managedwifi.activities.ProfileActivity
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ProfileFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {
    private val TAG = "ProfileFragment"
    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

//    private lateinit var apRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }


    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view=inflater!!.inflate(R.layout.fragment_profile, container, false)
        Log.d(TAG,"apRecyclerView: "+apRecyclerView);

        //adding a layoutmanager
        val linearLayoutManager=LinearLayoutManager(this.activity)
        linearLayoutManager.orientation=LinearLayoutManager.VERTICAL
        view.apRecyclerView.layoutManager =linearLayoutManager



        //crating an arraylist to store users using the data class user
        val users = ArrayList<Ap>()

        //adding some dummy data to the list
        users.add(Ap("Belal Khan", "Ranchi Jharkhand"))
        users.add(Ap("Ramiz Khan", "Ranchi Jharkhand"))
        users.add(Ap("Faiz Khan", "Ranchi Jharkhand"))
        users.add(Ap("Yashar Khan", "Ranchi Jharkhand"))
        users.add(Ap("Belal Khan", "Ranchi Jharkhand"))
        users.add(Ap("Ramiz Khan", "Ranchi Jharkhand"))
        users.add(Ap("Faiz Khan", "Ranchi Jharkhand"))
        users.add(Ap("Yashar Khan", "Ranchi Jharkhand"))
        users.add(Ap("Belal Khan", "Ranchi Jharkhand"))
        users.add(Ap("Ramiz Khan", "Ranchi Jharkhand"))
        users.add(Ap("Faiz Khan", "Ranchi Jharkhand"))
        users.add(Ap("Yashar Khan", "Ranchi Jharkhand"))

        //creating our adapter
        val adapter = CustomAdapter(users)

        //now adding the adapter to recyclerview
        view.apRecyclerView.adapter = adapter


        // Inflate the layout for this fragment
        return view
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

//    override fun onAttach(context: Context?) {
//        super.onAttach(context)
//        if (context is OnFragmentInteractionListener) {
//            mListener = context
//        } else {
//            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
//        }
//    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment ProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): ProfileFragment {
            val fragment = ProfileFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }



    inner class SsidRecyclerViewAdapter( val context: Context): RecyclerView.Adapter<SsidRecyclerViewAdapter.SsidViewHolder>() {



        override fun getItemCount(): Int {
            return 10;
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= SsidViewHolder( LayoutInflater.from(parent.context).inflate(R.layout.settings_ssid_row_item,parent))

        override fun onBindViewHolder(holder: SsidViewHolder, position: Int) {
            holder.bindItems()
        }


        inner class  SsidViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bindItems() {

            }
        }
    }


    class CustomAdapter(val userList: ArrayList<Ap>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

        //this method is returning the view for each item in the list
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.profile_ap_item, parent, false)
            return ViewHolder(v)
        }

        //this method is binding the data on the list
        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bindItems(userList[position])
        }

        //this method is giving the size of the list
        override fun getItemCount(): Int {
            return userList.size
        }

        //the class is hodling the list view
        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            fun bindItems(user: Ap) {

            }
        }
    }

    data class Ap(val name: String, val address: String)


}// Required empty public constructor
