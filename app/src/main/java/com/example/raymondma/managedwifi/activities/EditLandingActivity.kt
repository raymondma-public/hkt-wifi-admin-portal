package com.example.raymondma.managedwifi.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.example.raymondma.managedwifi.R
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v4.view.PagerAdapter
import com.example.raymondma.managedwifi.fragments.*
import kotlinx.android.synthetic.main.activity_edit_landing.*


class EditLandingActivity : AppCompatActivity() {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private val NUM_PAGES = 5

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private lateinit var mPager: ViewPager

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private var mPagerAdapter: PagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_landing)

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = pager as ViewPager
        mPagerAdapter = ScreenSlidePagerAdapter(getSupportFragmentManager())
        mPager.adapter=mPagerAdapter

    }

    override fun onBackPressed() {
        if (mPager.currentItem === 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed()
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1)
        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            var fragments=Array<Fragment>(5){EditLandingPageFragment();EditPromotionFragment();EditNoticeBoardFragment();EditMembershipFragment();EditSocialMediaFragment()}


            return fragments[position]
        }

        override fun getCount(): Int {
            return NUM_PAGES
        }
    }

}
