package com.example.raymondma.managedwifi.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.raymondma.managedwifi.R

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
    fun login(view: View?){
        var i: Intent = Intent(this, ProfileActivity::class.java)
        startActivity(i)

    }
}
