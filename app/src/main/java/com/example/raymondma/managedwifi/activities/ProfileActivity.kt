package com.example.raymondma.managedwifi.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import com.example.raymondma.managedwifi.R
import com.example.raymondma.managedwifi.fragments.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.menu_list_item.view.*
import org.jetbrains.anko.image
import org.jetbrains.anko.onClick

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
//        var menuItemsString=arrayOf("Profile", "Settings", "Landing Page", "Reports", "UCS Admin Portal", "Log out").asList()
        var menuIcons = arrayOf(R.drawable.nav_profile_0, R.drawable.nav_setting_0, R.drawable.nav_landing_0, R.drawable.nav_socialconnect_0, R.drawable.nav_reports_0, R.drawable.nav_ucs_btn_0, R.drawable.nav_logout_0).asList()
        left_drawer.setAdapter(DrawerAdapter(menuIcons, this))


    }


    inner class DrawerAdapter(val list: List<Int>, val context: Context) : BaseAdapter() {
        override fun getCount(): Int {
            return list.size
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getItem(position: Int): Any {
            return list.get(position)
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

            var v: View
            if (convertView == null) {
                v = View.inflate(context, R.layout.menu_list_item, null)

            } else {
                v = convertView

            }

            v.menuItemIcon.image = context.resources.getDrawable(list[position])
            v.menuItemIcon.onClick {
                //                val i=Intent(context , SettingActivity::class.java)
//                context.startActivity(i)

                var activity = context as AppCompatActivity;

                val ft = activity.supportFragmentManager.beginTransaction()
                when (list[position]) {
                    R.drawable.nav_profile_0 -> ft.replace(R.id.contentFragment, ProfileFragment())
                    R.drawable.nav_setting_0 -> ft.replace(R.id.contentFragment, SettingFragment())
                    R.drawable.nav_landing_0 -> ft.replace(R.id.contentFragment, PortalLandingFragment())
                    R.drawable.nav_socialconnect_0 -> ft.replace(R.id.contentFragment, SocialConnectFragment())
                    R.drawable.nav_reports_0 -> ft.replace(R.id.contentFragment, ReportFragment())
                    R.drawable.nav_ucs_btn_0 -> ft.replace(R.id.contentFragment, UcsFragment())
                    R.drawable.nav_logout_0 -> ft.replace(R.id.contentFragment, SettingFragment())


                }
                ft.commit()
                val leftDrawer = activity.drawer_layout as DrawerLayout
                leftDrawer.closeDrawers()
            }

            return v

        }

    }

}
